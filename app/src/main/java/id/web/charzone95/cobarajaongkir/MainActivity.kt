package id.web.charzone95.cobarajaongkir

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.AppCompatSpinner
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import butterknife.BindString
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.OnItemSelected
import id.web.charzone95.cobarajaongkir.responses.City
import id.web.charzone95.cobarajaongkir.responses.Province
import id.web.charzone95.cobarajaongkir.service.RajaOngkirService
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    val listProvinsiId : MutableList<String> = ArrayList<String>()
    val listProvinsiNama : MutableList<String> = ArrayList<String>()
    
    val listKotaAsalId : MutableList<String> = ArrayList<String>()
    val listKotaAsalNama : MutableList<String> = ArrayList<String>()
    
    val listKotaTujuanId : MutableList<String> = ArrayList<String>()
    val listKotaTujuanNama : MutableList<String> = ArrayList<String>()

    val listKurirKode: MutableList<String> = ArrayList<String>()
    val listKurirNama: MutableList<String> = ArrayList<String>()

    val adapterProvinsi by lazy {
        ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listProvinsiNama)
    }
    
    val adapterKotaAsal by lazy { 
        ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listKotaAsalNama)
    }
    
    val adapterKotaTujuan by lazy { 
        ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listKotaTujuanNama)
    }

    val adapterKurir by lazy {
        ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listKurirNama)
    }
    
    

    @BindString(R.string.pick_province)
    lateinit var pickProvince : String

    @BindString(R.string.pick_city)
    lateinit var pickCity : String

    @BindString(R.string.failedToLoadData)
    lateinit var failedToLoadData : String

    val rajaOngkirService by lazy {
        RajaOngkirService.create()
    }
    
    

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ButterKnife.bind(this)

        //hide yg kota
        spinnerAsalKota.visibility = View.GONE
        spinnerTujuanKota.visibility = View.GONE

        initDataSpinnerProvinsi()

        setupSpinnerKota()

        setupSpinnerKurir()
    }

    private fun initDataSpinnerProvinsi() {
        adapterProvinsi.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerAsalProvinsi.adapter = adapterProvinsi
        spinnerTujuanProvinsi.adapter = adapterProvinsi

        rajaOngkirService.listProvince(RajaOngkirService.API_KEY).enqueue(object : Callback<Province.ResponseProvince> {
            override fun onFailure(call: Call<Province.ResponseProvince>?, t: Throwable?) {
                Toast.makeText(this@MainActivity, failedToLoadData, Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<Province.ResponseProvince>?, response: Response<Province.ResponseProvince>?) {
                val hasil = response?.body()

                listProvinsiId.clear()
                listProvinsiNama.clear()

                listProvinsiNama.add(pickProvince)
                listProvinsiId.add("")

                hasil?.rajaongkir?.results.let {
                    for (item in hasil?.rajaongkir?.results!!) {
                        listProvinsiId.add(item.provinceId)
                        listProvinsiNama.add(item.province)
                    }
                }

                adapterProvinsi.notifyDataSetChanged()

            }
        })
    }

    fun setupSpinnerKota() {
        adapterKotaAsal.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerAsalKota.adapter = adapterKotaAsal
        
        adapterKotaTujuan.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerTujuanKota.adapter = adapterKotaTujuan
    }


    @OnItemSelected(R.id.spinnerAsalProvinsi)
    fun provinsiAsalSelected(position: Int) {
        val selectedId = listProvinsiId.get(position)

        if (selectedId != "") {
            rajaOngkirService.listCity(RajaOngkirService.API_KEY, selectedId).enqueue(object : Callback<City.ResponseCity> {
                override fun onResponse(call: Call<City.ResponseCity>?, response: Response<City.ResponseCity>?) {
                    val hasil = response?.body()

                    listKotaAsalId.clear()
                    listKotaAsalNama.clear()

                    listKotaAsalNama.add(pickCity)
                    listKotaAsalId.add("")

                    hasil?.rajaongkir?.results.let {
                        for (item in hasil?.rajaongkir?.results!!) {
                            listKotaAsalId.add(item.cityId)
                            listKotaAsalNama.add(item.cityName)
                        }
                    }

                    adapterKotaAsal.notifyDataSetChanged()
                    spinnerAsalKota.visibility = View.VISIBLE
                    spinnerAsalKota.setSelection(0)
                }

                override fun onFailure(call: Call<City.ResponseCity>?, t: Throwable?) {
                    Toast.makeText(this@MainActivity, failedToLoadData, Toast.LENGTH_SHORT).show()
                }
            })
        }
    }
    
    @OnItemSelected(R.id.spinnerTujuanProvinsi)
    fun provinsiTujuanSelected(position: Int) {
        val selectedId = listProvinsiId.get(position)

        if (selectedId != "") {
            rajaOngkirService.listCity(RajaOngkirService.API_KEY, selectedId).enqueue(object : Callback<City.ResponseCity> {
                override fun onResponse(call: Call<City.ResponseCity>?, response: Response<City.ResponseCity>?) {
                    val hasil = response?.body()

                    listKotaTujuanId.clear()
                    listKotaTujuanNama.clear()

                    listKotaTujuanNama.add(pickCity)
                    listKotaTujuanId.add("")

                    hasil?.rajaongkir?.results.let {
                        for (item in hasil?.rajaongkir?.results!!) {
                            listKotaTujuanId.add(item.cityId)
                            listKotaTujuanNama.add(item.cityName)
                        }
                    }

                    adapterKotaTujuan.notifyDataSetChanged()
                    spinnerTujuanKota.visibility = View.VISIBLE
                    spinnerTujuanKota.setSelection(0)
                }

                override fun onFailure(call: Call<City.ResponseCity>?, t: Throwable?) {
                    Toast.makeText(this@MainActivity, failedToLoadData, Toast.LENGTH_SHORT).show()
                }
            })
        }
    }


    fun setupSpinnerKurir() {
        adapterKurir.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerKurir.adapter = adapterKurir

        listKurirKode.add("jne")
        listKurirKode.add("pos")
        listKurirKode.add("tiki")

        listKurirNama.add("JNE")
        listKurirNama.add("POS Indonesia")
        listKurirNama.add("TIKI")

        adapterKurir.notifyDataSetChanged()
        spinnerKurir.setSelection(0)
    }

    @OnItemSelected(R.id.spinnerAsalKota, R.id.spinnerTujuanKota)
    fun spinnerKotaAsalChanged() {
        var selectedKotaAsal = ""
        var selectedKotaTujuan = ""
        
        if (listKotaAsalId.isNotEmpty() && spinnerAsalKota.selectedItemPosition != -1) {
            selectedKotaAsal = listKotaAsalId.get(spinnerAsalKota.selectedItemPosition)
        }
        
        if (listKotaTujuanId.isNotEmpty() && spinnerTujuanKota.selectedItemPosition != -1) {
            selectedKotaTujuan = listKotaTujuanId.get(spinnerTujuanKota.selectedItemPosition)
        }
        
        
        buttonOK.isEnabled = selectedKotaAsal.isNotEmpty() && selectedKotaTujuan.isNotEmpty()
    }

    @OnClick(R.id.buttonOK)
    fun buttonOKClick() {
        val kotaAsalId = listKotaAsalId.get(spinnerAsalKota.selectedItemPosition)
        val kotaTujuanId = listKotaTujuanId.get(spinnerTujuanKota.selectedItemPosition)
        val kurir = listKurirKode.get(spinnerKurir.selectedItemPosition)

        val intent = Intent(this, OngkirResultActivity::class.java)
        intent.putExtra(OngkirResultActivity.INTENT_KEY_ORIGIN, kotaAsalId)
        intent.putExtra(OngkirResultActivity.INTENT_KEY_DESTINATION, kotaTujuanId)
        intent.putExtra(OngkirResultActivity.INTENT_KEY_COURIER, kurir)

        startActivity(intent)
    }
}

package id.web.charzone95.cobarajaongkir.responses

import com.google.gson.annotations.SerializedName

class City {
    data class Status(
            val code: Int = 0,
            val description: String = ""
    )


    data class Query(
            val province: String = ""
    )


    data class ResultsItem(
            @SerializedName("city_name")
            val cityName: String = "",

            val province: String = "",

            @SerializedName("province_id")
            val provinceId: String = "",

            val type: String = "",

            @SerializedName("postal_code")
            val postalCode: String = "",

            @SerializedName("city_id")
            val cityId: String = ""
    )


    data class Rajaongkir(
            val query: Query?,
            val results: List<ResultsItem>?,
            val status: Status?
    )


    data class ResponseCity(
            val rajaongkir: Rajaongkir?
    )

}
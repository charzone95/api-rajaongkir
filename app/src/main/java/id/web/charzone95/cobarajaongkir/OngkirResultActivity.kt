package id.web.charzone95.cobarajaongkir

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import butterknife.BindString
import butterknife.ButterKnife
import id.web.charzone95.cobarajaongkir.responses.Cost
import id.web.charzone95.cobarajaongkir.service.RajaOngkirService
import kotlinx.android.synthetic.main.activity_ongkir_result.*
import kotlinx.android.synthetic.main.item_result.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OngkirResultActivity : AppCompatActivity() {
    companion object {
        const val INTENT_KEY_ORIGIN = "origin"
        const val INTENT_KEY_DESTINATION = "destination"
        const val INTENT_KEY_COURIER = "courier"
    }

    var intentOrigin = ""
    var intentDestination = ""
    var intentCourier = ""

    val weight = 1000

    val toolbar by lazy {
        toolbarResult
    }

    val rajaOngkirService by lazy {
        RajaOngkirService.create()
    }


    @BindString(R.string.failedToLoadData)
    lateinit var failedToLoadData : String

    var listData: MutableList<Cost.CostsItem> = ArrayList<Cost.CostsItem>()
    val adapter by lazy {
        DataAdapter(listData)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ongkir_result)
        ButterKnife.bind(this)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        setTitle("")

        recyclerViewData.adapter = adapter

        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recyclerViewData.layoutManager = layoutManager

        val intent = getIntent()
        intent.let { 
            intentOrigin = intent.getStringExtra(INTENT_KEY_ORIGIN)
            intentDestination = intent.getStringExtra(INTENT_KEY_DESTINATION)
            intentCourier = intent.getStringExtra(INTENT_KEY_COURIER)

            loadOngkirData()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }

        return super.onOptionsItemSelected(item)
    }

    fun loadOngkirData() {
        rajaOngkirService.getCost(RajaOngkirService.API_KEY, intentOrigin, intentDestination, weight, intentCourier).enqueue(object : Callback<Cost.ResponseCost> {
            override fun onResponse(call: Call<Cost.ResponseCost>?, response: Response<Cost.ResponseCost>) {
                val hasil: Cost.Rajaongkir? = response.body()?.rajaongkir

                try {
                    if (hasil == null) {
                        throw Exception()
                    }

                    val originName = hasil.originDetails?.cityName
                    val destinationName = hasil.destinationDetails?.cityName

                    var courierName = ""
                    val costResult = hasil.results?.get(0)
                    if (costResult != null) {
                        courierName = costResult.name
                    }

                    toolbar.setTitle(courierName)
                    toolbar.setSubtitle(originName + " → " + destinationName)

                    adapter.setData(hasil.results?.get(0)?.costs!!)
                    adapter.notifyDataSetChanged()

                } catch (e: Exception) {
                    Toast.makeText(this@OngkirResultActivity, failedToLoadData, Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<Cost.ResponseCost>?, t: Throwable?) {
                Toast.makeText(this@OngkirResultActivity, failedToLoadData, Toast.LENGTH_SHORT).show()
            }
        })
    }

    class DataViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(data: Cost.CostsItem) {
            itemView.textViewService.text = data.service
            itemView.textViewDescription.text = data.description
            itemView.textViewCost.text = "Rp " + data.cost?.get(0)?.value
            itemView.textViewEtd.text = data.cost?.get(0)?.etd + " hari"
        }
    }

    class DataAdapter(data: List<Cost.CostsItem>) : RecyclerView.Adapter<DataViewHolder>() {
        var listData: List<Cost.CostsItem> = data

        fun setData(data: List<Cost.CostsItem>) {
            listData = data
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(
                    R.layout.item_result, parent, false
            )

            return DataViewHolder(view)
        }

        override fun onBindViewHolder(holder: DataViewHolder, position: Int) {
            holder.bind(listData.get(position))
        }

        override fun getItemCount(): Int {
            return listData.size
        }

    }
}

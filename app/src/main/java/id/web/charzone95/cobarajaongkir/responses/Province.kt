package id.web.charzone95.cobarajaongkir.responses


import com.google.gson.annotations.SerializedName

class Province {
    data class Status(
            val code: Int = 0,
            val description: String = ""
    )


    data class ResultsItem(
            val province: String = "",

            @SerializedName("province_id")
            val provinceId: String = ""
    )


    data class Rajaongkir(
            val results: List<ResultsItem>?,
            val status: Status?
    )


    data class ResponseProvince(
            val rajaongkir: Rajaongkir?
    )

}
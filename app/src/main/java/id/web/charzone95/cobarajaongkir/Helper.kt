package id.web.charzone95.cobarajaongkir

import android.content.Context
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

fun getRetrofit(baseUrl: String, debugMode: Boolean = false) : Retrofit {
    val builder =  Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(baseUrl)

    val httpClient = OkHttpClient.Builder()

    if (debugMode) {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY

        httpClient.addInterceptor(interceptor)
    }

    builder.client(httpClient.build())

    return builder.build()
}

package id.web.charzone95.cobarajaongkir.service

import id.web.charzone95.cobarajaongkir.getRetrofit
import id.web.charzone95.cobarajaongkir.responses.City
import id.web.charzone95.cobarajaongkir.responses.Cost
import id.web.charzone95.cobarajaongkir.responses.Province
import retrofit2.Call
import retrofit2.http.*

interface RajaOngkirService {

    @GET("province")
    fun listProvince(@Header("key") apiKey: String): Call<Province.ResponseProvince>

    @GET("city")
    fun listCity(@Header("key") apiKey: String, @Query("province") provinceId: String): Call<City.ResponseCity>

    @POST("cost")
    @FormUrlEncoded
    fun getCost(
            @Header("key") apiKey: String,
            @Field("origin") originId: String,
            @Field("destination") destinationId: String,
            @Field("weight") weight: Int,
            @Field("courier") courier: String
    ) : Call<Cost.ResponseCost>

    companion object {
        val BASE_URL = "https://api.rajaongkir.com/starter/"
        val API_KEY = "82f7818bd2716948495e551fa4e29bf1"

        fun create() : RajaOngkirService {
            return getRetrofit(BASE_URL).create(RajaOngkirService::class.java)
        }
    }
}
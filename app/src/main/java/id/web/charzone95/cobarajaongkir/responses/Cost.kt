package id.web.charzone95.cobarajaongkir.responses


import com.google.gson.annotations.SerializedName

class Cost {

    data class Status(
            val code: Int = 0,
            val description: String = ""
    )


    data class Query(
            val courier: String = "",
            val origin: String = "",
            val destination: String = "",
            val weight: Int = 0
    )


    data class OriginDetails(
            @SerializedName("city_name")
            val cityName: String = "",

            val province: String = "",

            @SerializedName("province_id")
            val provinceId: String = "",

            val type: String = "",

            @SerializedName("postal_code")
            val postalCode: String = "",

            @SerializedName("city_id")
            val cityId: String = ""
    )


    data class CostItem(
            val note: String = "",
            val etd: String = "",
            val value: Int = 0
    )


    data class ResultsItem(
            val costs: List<CostsItem>?,
            val code: String = "",
            val name: String = ""
    )


    data class Rajaongkir(
            val query: Query?,

            @SerializedName("destination_details")
            val destinationDetails: DestinationDetails?,

            @SerializedName("origin_details")
            val originDetails: OriginDetails?,

            val results: List<ResultsItem>?,

            val status: Status?
    )


    data class CostsItem(
            val cost: List<CostItem>?,
            val service: String = "",
            val description: String = ""
    )


    data class ResponseCost(
            val rajaongkir: Rajaongkir?
    )


    data class DestinationDetails(
            @SerializedName("city_name")
            val cityName: String = "",

            val province: String = "",

            @SerializedName("province_id")
            val provinceId: String = "",

            val type: String = "",

            @SerializedName("postal_code")
            val postalCode: String = "",

            @SerializedName("city_id")
            val cityId: String = ""
    )

}